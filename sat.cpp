#include "parser.h"
#include <iostream>
#include <fstream>
#include <cmath>
#include <stack>
#include <vector>
#include <string>
#include <set>
#include <queue>
#include <utility>
#include <cassert>
#include <algorithm>
#include <functional>
using namespace std;
template <class It>                                                         
inline size_t hash_range(It first, It last)
{
    size_t seed = 0;
    hash<typename It::value_type> hasher;                                                  
    for(; first != last; ++first)
        seed ^= hasher(*first) + 0x9e3779b9 + (seed<<6) + (seed>>2);                 
    return seed;
}
class Solver
{
public:
    void add_watch(int i) {
        vector<int> const& clause = clauses[i];
        if (clause.size() < 2)
            return;
        if (clause[0] > 0) pos_watched[clause[0]].insert(i);
        else neg_watched[-clause[0]].insert(i);
        if (clause[1] > 0) pos_watched[clause[1]].insert(i);
        else neg_watched[-clause[1]].insert(i);
    }
    Solver(const char *file) {
        parse_DIMACS_CNF(clauses, maxVarIndex, file);
        ans = vector<int>(maxVarIndex + 1, -1);
        pos_watched.resize(maxVarIndex + 1);
        neg_watched.resize(maxVarIndex + 1);
        clean();
        for (unsigned int i = 0; i < clauses.size(); i++)
            add_watch(i);
    }
    bool solve() {
        return DPLL();
    }
    void print(ofstream& out) {
        out << "v ";
        for (vector<int>::size_type i = 1; i < ans.size(); i++) {
            if (ans[i]) out << i;
            else out << static_cast<int>(-i);
            out << " ";
        }
        out << "0" << std::endl;
    }
    int verify() {
        for (vector<int> const& clause : clauses) {
            bool success = false;
            for (int lit : clause) {
                if (ans[abs(lit)] == -1)
                    return 0;
                if ((lit > 0 && ans[lit]) ||
                    (lit < 0 && !ans[-lit])) {
                    success = true;
                    break;
                }
            }
            if (!success)
                return -1;
        }
        return 1;
    }
private:
    int literal_update(set<int>& watch) {
        vector<int> toDelete;
        vector<pair<int, int>> move, temp;
        for (auto i : watch) {
            int find = 0, imply = 0;
            for (int x : clauses[i]) {
                if ((x > 0 && ans[x] == 1) ||
                    (x < 0 && ans[-x] == 0)) {
                    find = 1;
                    break;
                }
            }
            // resolved
            if (find)
                continue;
            for (int x : clauses[i]) {
                int ax = abs(x);
                if (ans[ax] == -1) {
                    if (pos_watched[ax].find(i) != pos_watched[ax].end() ||
                        neg_watched[ax].find(i) != neg_watched[ax].end()) {
                        imply = x;
                    }
                    else {
                        toDelete.push_back(i);
                        move.push_back(make_pair(x, i));
                        find = 1;
                        break;
                    }
                }
            }
            if (find)
                continue;
            if (imply == 0)
                return i;
            temp.push_back(make_pair(imply, i));
        }
        for (auto& q : temp)
            assign.push(q);
        for (auto i : toDelete)
            watch.erase(i);
        for (auto& p : move)
            if (p.first > 0)
                pos_watched[p.first].insert(p.second);
            else
                neg_watched[-p.first].insert(p.second);
        return -1;
    }

    int choose_next() {
        int ret = 0;
        for (unsigned lit = 1; lit < ans.size(); lit++)
            if (ans[lit] == -1) {
                ret = lit;
                break;
            }
        return ret;
    }

    void clean() {
        while (!assign.empty())
            assign.pop();
    }

    void resolve(const vector<int>& C, const vector<int>& F, int x, vector<int>& ret) {
        set<int> tmp;
        for (int lit : C)
            tmp.insert(lit);
        for (int lit : F)
            tmp.insert(lit);
        tmp.erase(x);
        tmp.erase(-x);
        for (int lit : tmp)
            ret.push_back(lit);
    }

    void conflict_analysis(int ci) {
        vector<int> prev, ret = clauses[ci];
         for (auto rit = save.crbegin(), rend = save.crend(); rit != rend; ++rit) {
            int x = rit->first;
            vector<int>::const_iterator iter = find(clauses[ci].begin(), clauses[ci].end(), -x);
            if (iter != clauses[ci].end()) {
                prev = move(ret);
                resolve(prev, clauses[rit->second], x, ret);
            }
        }
        size_t hash = hash_range(ret.begin(), ret.end());
        auto ss = hash_cla.insert(hash);
        if (ss.second) {
            if (ret.size() == 1)
                return;
            clauses.push_back(ret);
            add_watch(clauses.size() - 1);
        }
    }

    bool DPLL() {

        if (!assign.empty()) {
            auto xs = assign.front();
            assign.pop();
            int x = abs(xs.first);
            bool sign = xs.first > 0;
            if (ans[x] == !sign) {
                clean();
                return false;
            }
            if (ans[x] == sign)
                return DPLL();
            save.push_back(xs);
            ans[x] = sign;
            int ret = literal_update(sign ? neg_watched[x] : pos_watched[x]);
            if (ret == -1) {
                if (DPLL())
                    return true;
            } else
                conflict_analysis(ret);
            save.pop_back();
            ans[x] = -1;
            clean();
            return false;
        }

        int n = choose_next();
        if (n == 0)
            return true;

        ans[n] = 1;
        if (literal_update(neg_watched[n]) == -1)
            if (DPLL())
                return true;

        ans[n] = 0;
        if (literal_update(pos_watched[n]) == -1)
            if (DPLL())
                return true;

        ans[n] = -1;
        return false;
    }
    int maxVarIndex;
    vector<vector<int>> clauses;
    vector<int> ans;
    vector<set<int>> pos_watched, neg_watched;
    queue<pair<int, int>> assign;
    vector<pair<int, int>> save;
    set<size_t> hash_cla;
};
int main(int argc, const char *argv[])
{
    if (argv[1] == NULL) {
        std::cerr << "Please specify input file" << std::endl;
        exit(EXIT_FAILURE);
    }
    Solver S(argv[1]);
    ofstream output;
    string filename(argv[1]);
    int size = filename.size();
    filename[size - 3] = 's';
    filename[size - 2] = 'a';
    filename[size - 1] = 't';
    output.open(filename);
    if (S.solve()) {
        output << "s SATISFIABLE" << std::endl;
        S.print(output);
    } else
        output << "s UNSATISFIABLE" << std::endl;
    output.close();
    return 0;
}
